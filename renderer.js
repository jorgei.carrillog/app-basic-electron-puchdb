window.addEventListener('DOMContentLoaded', () => 
{
    const electron = require('electron');
    const PouchDB = require('pouchdb');
    const toastr = require('toastr');
    const remote = electron.remote;

    const citizenDB = new PouchDB('citizens');

    document.getElementById('citizen-form').addEventListener('submit', async (e) => {
        e.preventDefault();
    
        // Captura los datos del formulario
        const id = document.getElementById('id').value;
        const nombres = document.getElementById('nombres').value;
        const apellidos = document.getElementById('apellidos').value;
        const direccion = document.getElementById('direccion').value;
        const estrato = document.getElementById('estrato').value;
        const celular = document.getElementById('celular').value;
        const correo = document.getElementById('correo').value;
    
        // Aquí puedes validar los datos si es necesario
    
        // Crea un objeto con los datos del ciudadano
        const citizenData = {
        _id : id,
        nombres:nombres,
        apellidos:apellidos,
        direccion:direccion,
        estrato:estrato,
        celular:celular,
        correo:correo
        };
    
        try {
        // Guarda los datos en la base de datos PouchDB
        await citizenDB.put(citizenData);
    
        // Limpia el formulario después de guardar
        document.getElementById('citizen-form').reset();
        } catch (error) {
        toastr.error('Error al guardar los datos:', error);
        }
    });  

    document.getElementById('view-records').addEventListener('click', async () => {
        try {
        // URL de tu servidor CouchDB
        const couchDBUrl = 'http://admin:1234@127.0.0.1:15984/db-citizen';
    
        // Inicia la replicación desde PouchDB a CouchDB
        const replicationToCouchDB = citizenDB.replicate.from(couchDBUrl);
    
        replicationToCouchDB.on('complete', () => {
        toastr.success('Sincronización a CouchDB completada.');
        });
    
        replicationToCouchDB.on('error', (err) => {
        toastr.error('Error en la sincronización a CouchDB:', err);
        });
        // Recupera todos los registros de ciudadanos desde la base de datos PouchDB
        const result = await citizenDB.allDocs({ include_docs: true });
        const citizens = result.rows.map(row => row.doc);
    
        // Aquí puedes procesar la lista de ciudadanos como desees, por ejemplo, mostrarlos en una tabla
        const recordsContainer = document.getElementById('records-container');
        recordsContainer.innerHTML = ''; // Limpia el contenedor antes de mostrar los nuevos registros
    
        if (citizens.length > 0) {
            const table = document.createElement('table');
            table.innerHTML = `
            <tr>
                <th>Número de Identificación</th>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Dirección</th>
                <th>Estrato</th>
                <th>Celular</th>
                <th>Correo Electrónico</th>
            </tr>
            `;
    
            citizens.forEach(citizen => {
            const row = document.createElement('tr');
            row.innerHTML = `
                <td>${citizen._id}</td>
                <td>${citizen.nombres}</td>
                <td>${citizen.apellidos}</td>
                <td>${citizen.direccion}</td>
                <td>${citizen.estrato}</td>
                <td>${citizen.celular}</td>
                <td>${citizen.correo}</td>
            `;
            table.appendChild(row);
            });
    
            recordsContainer.appendChild(table);
        } else {
            recordsContainer.textContent = 'No hay registros disponibles.';
        }
        } catch (error) {
        toastr.error('Error al obtener los registros:', error);
        }
    });

    document.getElementById('sync').addEventListener('click', () => {
        // URL de tu servidor CouchDB
        const couchDBUrl = 'http://admin:1234@127.0.0.1:15984/db-citizen';
    
        // Inicia la replicación desde PouchDB a CouchDB
        const replicationToCouchDB = citizenDB.replicate.to(couchDBUrl);
    
        replicationToCouchDB.on('complete', () => {
        toastr.success('Sincronización a CouchDB completada.');
        });
    
        replicationToCouchDB.on('error', (err) => {
        toastr.error('Error en la sincronización a CouchDB:', err);
        });
    });  
});  
    
