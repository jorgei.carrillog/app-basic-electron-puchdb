const { app, BrowserWindow } = require('electron');
const path = require('node:path')

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({ width: 1360, height: 1080,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false,
            contentSecurityPolicy: "default-src 'self' data:; img-src 'self' data:"
        }
    });
  mainWindow.loadFile('index.html');
  // Abre las herramientas de desarrollador
  mainWindow.webContents.openDevTools();

  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
